# Requirements:
```
python 3.6
pytorch 0.4.1
jupyter lab
cnn_finetune
```

# Installation
```
pip install cnn_finetune
```

# Download dataset and train
```
chmod +x run_finetune_zalo_landmark.sh
./run_finetune_zalo_landmark.sh
```
# Prediction on test set
```
python predict_all.py
```
Note: For private testset, you should change the path of test set at: 
**predict_all.py#L45**

# Make submission
```
jupyter lab
make_submit.ipynb
```
The output is `4models_full_gmean.csv`